import logo from './logo.svg';
import React, { useEffect, useState } from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Blogs from './Blogs';
import Login from './Login';
import {List, AddBlog, Blog} from "./AdminPanel/AdminPanel";
import { fetchBlogs, subscribe, fetchCurrentUser } from "./utils";
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {
  Navbar,
  Nav,
  Button,
  Container
} from "react-bootstrap";

function App() {
  const [user,setUser] = useState({});
  const [blogs, setBlogs] = useState([]);

  useEffect(() => {
    fetchCurrentUser().then(res=>setUser(res));
    fetchBlogs().then(res=>setBlogs(res.documents))
  },[]);

  useEffect(() => {
    const unsubscribe = subscribe(setBlogs);
    return function cleanup() {
      unsubscribe();
    };
  },[]);

  return (
    <div className="App">
      <Router>
      <div>
        <>
          <Navbar bg="dark" variant="dark">
            <Container>
              <Navbar.Brand href="/">
                My Blog
              </Navbar.Brand>
              <Nav className="me-auto">
                <Nav.Link href="/">Home</Nav.Link>
                <Nav.Link href="/admin">Manage Blogs</Nav.Link>
                {/* <Nav.Link href="/login"></Nav.Link> */}
              </Nav>
                <Button style={{marginLeft:'auto'}} variant="primary" href="/admin/add">Write a Blog</Button>
            </Container>
          </Navbar>
        </>

        <Switch>
          <Route path="/login">
            <Login />
          </Route>
          
          <Route exact path="/">
            <Blogs blogs={blogs} />
          </Route>

          <Route exact path="/admin" render={() => <List blogs={blogs.filter(({$permissions})=>{console.log($permissions.write.includes("user:"+user.$id), $permissions.write, user.$id); return $permissions.write.includes("user:"+user.$id);})} />} />        
        <Route path="/admin/add" component={AddBlog} />
        <Route exact path="/blog/:id" render={({match}) => (
            <Blog blog={blogs.find(({$id}) => $id === match.params.id)} /> )}
        />

        <Route path="/blog/:id/edit" render={({match}) => (
            <AddBlog blog={blogs.find(({$id}) => $id === match.params.id)} /> )}
        />
        </Switch>

          <ToastContainer 
            position="bottom-right"
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop
            closeOnClick={false}
            rtl={false}
            pauseOnFocusLoss={false}
            draggable
            pauseOnHover
          />
      </div>
    </Router>

    </div>
  );
}

export default App;
