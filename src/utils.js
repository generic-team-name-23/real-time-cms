import React, { useEffect, useState } from 'react';
import { Appwrite } from "appwrite";
import { ENDPOINT, PROJECT_ID, COLLECTION_ID } from "./config";
import { toast } from 'react-toastify';
import {
    ToastContainer,
    Toast
} from "react-bootstrap";

const appwrite = new Appwrite();

appwrite
    .setEndpoint(ENDPOINT)
    .setProject(PROJECT_ID)
;

const subscribe = (setBlogs) => {
    return appwrite.subscribe([`collections.${COLLECTION_ID}.documents`], response => {
        // Callback will be executed on changes for documents A and all files.
        console.log(response);
        fetchBlogs().then(res=>setBlogs(res.documents));
        if(response.event=='database.documents.create')
            toast("A new blog just published!");
        else if(response.event=='database.documents.update')
            toast("This article was just updated!");
    });
}

const fetchBlogs = () => {
    console.log("hi");
    let promise = appwrite.database.listDocuments(COLLECTION_ID,[],100,0,'$id','DESC');
    return promise.then(function (res) {
        console.log(res)
        return res; // Success
    }, function (error) {
        return error; // Failure
    });
} 

const fetchMyBlogs = () => {
    let promise = appwrite.database.listDocuments(COLLECTION_ID);

    promise.then(function (res) {
        return res.documents; // Success
    }, function (error) {
        return error; // Failure
    });
} 

const getBlogById = (documentId) => {
    let promise = appwrite.database.getDocument(COLLECTION_ID, documentId);

    return promise.then(function (response) {
        console.log(response);
        return response; // Success
    }, function (error) {
        console.log(error); // Failure
        return []
    });
}

const addBlog = (heading, body) => {
    let promise = appwrite.database.createDocument(COLLECTION_ID, {heading, body}, ["*"]);

    return promise.then(function (response) {
        console.log(response); // Success
    }, function (error) {
        console.log(error); // Failure
    });
}

const updateBlog = (documentId, heading, body) => {
    let promise = appwrite.database.updateDocument(COLLECTION_ID, documentId, {heading, body});

    promise.then(function (response) {
        console.log(response); // Success
    }, function (error) {
        console.log(error); // Failure
    });  
}

const deleteBlog = (documentId) => {
    let promise = appwrite.database.deleteDocument(COLLECTION_ID, documentId);

    return promise.then(function (response) {
        console.log(response); // Success
    }, function (error) {
        console.log(error); // Failure
    });
}

const ToastComponent = (text) => {
    const [show, setShow] = useState(true);

    return (
    <Toast onClose={() => setShow(false)} show={show} delay={10000} autohide>
      <Toast.Header>
        <img src="holder.js/20x20?text=%20" className="rounded me-2" alt="" />
        <strong className="me-auto">Bootstrap</strong>
        <small className="text-muted">just now</small>
      </Toast.Header>
      <Toast.Body>{ text }</Toast.Body>
    </Toast>)
}

const fetchCurrentUser = () => {
    let promise = appwrite.account.get();
    return promise.then(function (response) {
        console.log(response); // Success
        return response;
    }, function (error) {
        console.log(error); // Failure
        return error;
    });
}

export {fetchBlogs, fetchMyBlogs, getBlogById, addBlog, updateBlog, deleteBlog, ToastComponent, subscribe, fetchCurrentUser};