import React, { useEffect, useState, useRef, useCallback  } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  withRouter,
  useHistory
} from "react-router-dom";
import {fetchBlogs, getBlogById, addBlog, updateBlog, deleteBlog} from "../utils";
import { Form, Button, ButtonGroup, Card } from "react-bootstrap";
import { FiEye } from 'react-icons/fi';
import { MdModeEditOutline, MdDeleteOutline } from 'react-icons/md';

const List = ({blogs}) => {

    useEffect(() => {
      console.log(blogs);
  },[blogs]);

    return (
        <div>
        {
            blogs.map((blog, index) => {
                return (
                    <div key={index}>
                      <Card style={{width:'80vw',margin:'1rem auto'}}>
                        <Card.Body>
                          <div style={{display:'flex'}}>
                            <span style={{fontSize:'25px',fontWeight:'600'}}>{blog.heading}</span>
                            <span style={{marginLeft:'auto'}}>
                              <Button variant="outline-primary" href={"/blog/"+blog.$id}>View <FiEye /></Button> 
                              <Button variant="outline-primary" href={"/blog/"+blog.$id+"/edit"}>Edit <MdModeEditOutline/></Button>
                              <Button variant="outline-danger" onClick={()=>{ deleteBlog(blog.$id)}}>Delete <MdDeleteOutline/></Button>
                            </span>
                          </div>
                        </Card.Body>
                      </Card>
                    </div>
                )
            })
        }
        </div>
    );
}

const AddBlog = ({blog}) => {
  const [heading, setHeading] = useState("")
  const [body, setBody] = useState("")
  const history = useHistory();

  useEffect(() => {
    if(blog) {
      
      setHeading(blog.heading);
      setBody(blog.body);
    }
  },[])

  const createOrUpdateBlog = () => {
    if(blog) {
      console.log("hiii", {heading, body});
      updateBlog(blog.$id, heading, body); 
    }
    else 
      addBlog(heading, body).then(() => {history.push('/admin')});
    
  }

  return (
      <Form style={{width:'90vw', margin:'auto'}}>
        <Form.Group className="mb-3" controlId="formBasicHeading">
          <Form.Label><h3>Heading</h3></Form.Label>
          <Form.Control type="heading" placeholder="Enter heading" 
          onChange={(e)=>setHeading(e.target.value)} defaultValue={heading}/>
          <Form.Text className="text-muted">
          </Form.Text>
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicBody">
          <Form.Label><h3>Body</h3></Form.Label>
          <Form.Control as="textarea" rows={20} placeholder="Body" 
          onChange={(e)=>setBody(e.target.value)} defaultValue={body}
          />
        </Form.Group>
        <Button variant="primary" onClick={()=>{ createOrUpdateBlog()}}>{blog?"Update":"Create"}
        </Button>
        <br />
        { blog && <Button variant="danger" onClick={()=>{ deleteBlog(blog.$id).then(()=>{history.push('/admin')})}}>Delete</Button>}
      </Form>
  )
}

const Blog = ({blog}) => {
    // const [blog, setBlog] = useState({});
    useEffect(() => {
      // getBlogById(id).then(res=>setBlog(res))
    },[])

    // useEffect(() => {
    //   console.log(blog);
    // },[blog])

    return (
        <div>
          {
            blog && 
            <div style={{width:'90vw', margin:'auto'}}>
              <h1 style={{margin:'3rem 0 2rem',textAlign:'center',fontSize:'60px'}}>{ blog.heading }</h1>
              <p>{ blog.body }</p>
            </div>
          }
          {
            !blog && <div className="page-wrap d-flex flex-row align-items-center">
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-12 text-center">
                        <span className="display-1 d-block">404</span>
                        <div className="mb-4 lead">The page you are looking for was not found.</div>
                        <Link to="/" className="btn btn-link">Back to Home</Link>
                    </div>
                </div>
            </div>
        </div>
          }
        </div>
    )
}

const AdminPanel = () => {
    return <Router>
    {/* <div>
      <ul>
        <li>
          <Link to="/login">Login</Link>
        </li>
        <li>
          <Link to="/blogs">Blogs</Link>
        </li>
      </ul>

      <hr /> */}
    
    <Switch>
        <Route exact path="/admin" component={List} />        
        <Route path="/admin/add" component={AddBlog} />
        <Route exact path="/blog/:id" render={({match}) => (
            <Blog id={match.params.id} /> )}
        />

        <Route path="/blog/:id/edit" render={({match}) => (
            <AddBlog id={match.params.id} /> )}
        />

      </Switch>
    {/* </div> */}
  </Router>
}

export {List, AddBlog, Blog};