import React, { useEffect, useState, useRef, useCallback  } from 'react';
import { Appwrite } from "appwrite";
import { ENDPOINT, PROJECT_ID, COLLECTION_ID } from "./config";
import { getToast } from './utils';
// import "./InfiniteScroll.css";
// import addBlogs from './load';
import { Card, Button, Container, Row, Col } from "react-bootstrap";
import logo from './logo.png';

// Init Web SDK
const appwrite = new Appwrite();

appwrite
    .setEndpoint(ENDPOINT)
    .setProject(PROJECT_ID)
;

let promise = 0;

// let promise = appwrite.account.createAnonymousSession();

// promise.then(function (response) {
//     console.log(response); // Success
// }, function (error) {
//     console.log(error); // Failure
// });

const Blogs = ({blogs}) => {    
    return (
    <div className="container m-auto">
            {/* <button onClick={()=>addBlogs()}>Add blog</button> */}
            {
                blogs.map((blog, index) => {
                    return <div key={ index }>
                        <Card style={{ width: '85vw', margin: '2rem auto' }}>
                        <Card.Body>
                            
                            <Container>
                                <Row>
                                    <Col md={2}>
                                    <Card.Img variant="top" src={logo} />
                                    </Col>
                                    <Col md={10}>
                                        <Card.Title>{ blog.heading }</Card.Title>
                                        <Card.Text>
                                        { blog.body && blog.body.substring(0,240)+ " " }
                                        <Card.Link href={"/blog/"+blog.$id }>Read More...</Card.Link>
                                        </Card.Text>
                                    </Col>
                                </Row>
                            </Container>
                        </Card.Body>
                        </Card>
                    </div>
                })
            }
    </div>)
}

export default Blogs;