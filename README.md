
# Real-Time CMS

[![Built  with Appwrtite](https://imgur.com/LGPerPU.png){width=200px}](https://appwrite.io/)

A blogging app to demonstrate how Appwrite's Real-time Service can be used to make CMS changes instant


## Run locally

1. Install Appwrite by following the [installation guide](https://appwrite.io/docs/installation).

2. Open the Appwrite console and create a new project.

3. Navigate to `Database` and add a new `Collection` called `Photos`.

4. Add the following rules and permissions:

#### Rules:

| Label     | Key        | Type      | Required | Array | Default Value |
| :-------- | :--------- | :-------- | :------- | :---- | :------------ | 
| Heading   | `heading`  | Text      | `true`   | false |               |
| Body      | `body`     | Text      | `false`  | false |               |

#### Permissions:

Read Access: `*`

Write Access: `*`

3. Clone this repo
```bash
  git clone https://github.com/SakshiUppoor/infinite-scroll-demo.git
  cd real-time-cms
```

4. Install dependencies
```bash
  npm install
```

5. From the Appwrite console, note down the `API Endpoint`, `Project ID` and `Collection ID` of the `Photos` Collection and enter these in `src/config.js`.

6. The project is ready to run! :rocket:
```bash
  npm start
```
    
## Acknowledgements

 - [Appwrite](https://appwrite.io/) - End-to-end Backend Server
 - [React Toastify](https://www.npmjs.com/package/react-toastify) - Adds toast notifications to your app with ease
